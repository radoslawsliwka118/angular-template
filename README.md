# AngularTemplate

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Linting
Run `ng lint --fix` to linting files.

## Config
* Configuration files are located in `src/config`.
* Language files are located in `src/assets/i18n`.
* Models are located in `src/core/models`.
* Services are located in `src/core/services`.
* Guards are located in `src/core/guards`.
### API config
`apiConfig.model.ts` contains the API paths that are used in `authService.service.ts`

### Theme config
`themeConfig.model.ts` contains a theme variable that can be set to `light` or `dark`

## Sidebar
The sidebar component has two angular inputs that must be passed in the html parent file.
### menuItems
`menuItems` is an `MenuItem` object ( `models/sidebar.model.ts` ) that contains the content of the sidebar. 
### menuTitle
`menuTitle` is a string that contains the title of the sidebar.