import { Component, OnInit } from '@angular/core';
import { ITEMS } from './sidebar-dashboard-items';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    menuItems
    
    constructor() {}
    
    ngOnInit() {
        this.menuItems = ITEMS;
    }

}
