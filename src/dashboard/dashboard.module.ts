import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from 'src/shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { Item1Component } from './content/item1.component';


@NgModule({
    declarations: [ DashboardComponent, Item1Component ],
    imports: [
        SharedModule,
        CommonModule,
        DashboardRoutingModule
    ]
})
export class DashboardModule { }
