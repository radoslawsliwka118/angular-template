import { MenuItem } from '../core/models/sidebar.model';

export const ITEMS: MenuItem[] = [
    {
        id: 1,
        label: 'DASHBOARD.SIDEBAR.MENU1',
        isTitle: true
    },
    {
        id: 2,
        label: 'DASHBOARD.SIDEBAR.ITEM1.TITLE',
        icon: 'fa fa-home',
        subItems: [
            {
                id: 3,
                label: 'DASHBOARD.SIDEBAR.ITEM1.SUBITEM1',
                link: 'page1',
                parentId: 2
            },
            {
                id: 4,
                label: 'DASHBOARD.SIDEBAR.ITEM1.SUBITEM2',
                link: 'page2',
                parentId: 2
            },
            {
                id: 5,
                label: 'DASHBOARD.SIDEBAR.ITEM1.SUBITEM3',
                link: 'page3',
                parentId: 2
            },
            {
                id: 6,
                label: 'DASHBOARD.SIDEBAR.ITEM1.SUBITEM4',
                link: 'page4',
                parentId: 2
            },
        ]
    },
    {
        id: 8,
        label: 'DASHBOARD.SIDEBAR.MENU2',
        isTitle: true
    },
    {
        id: 9,
        label: 'DASHBOARD.SIDEBAR.ITEM2',
        icon: 'fab fa-accessible-icon',
        link: 'page5',
    },
    {
        id: 10,
        label: 'DASHBOARD.SIDEBAR.ITEM3',
        icon: 'fas fa-address-card',
        link: 'page6',
        
    },
    {
        id: 11,
        label: 'DASHBOARD.SIDEBAR.ITEM4',
        icon: 'fas fa-archway',
        link: 'page7'
    },
    {
        id: 12,
        label: 'DASHBOARD.SIDEBAR.ITEM5.TITLE',
        icon: 'fas fa-asterisk',
        subItems: [
            {
                id: 13,
                label: 'DASHBOARD.SIDEBAR.ITEM5.SUBITEM1',
                link: 'page8',
                parentId: 12
            },
            {
                id: 14,
                label: 'DASHBOARD.SIDEBAR.ITEM5.SUBITEM2',
                link: 'page9',
                parentId: 12
            }
        ]
    }
];

