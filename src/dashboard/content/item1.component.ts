import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-dash-page1',
    template: `<div style="display: flex; justify-content: center;">Dashboard Subitem</div>`
})

export class Item1Component implements OnInit {

    constructor(public translate: TranslateService) {}

    ngOnInit(): void {}

}
