import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from '../../../core/models/sidebar.model';
import { TranslateService } from '@ngx-translate/core';
import { NavigationService } from 'src/core/services/navigation.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {

    @Input('menuItems') items: any; 
    @Input('menuTitle') title: string;

    constructor(public translate: TranslateService, public navigationService: NavigationService) {}

    ngOnInit(): void {
        this.navigationService.hasSidebar = true;
    }

    ngOnDestroy(): void {
        this.navigationService.hasSidebar = false;
    }

    hasItems(item: MenuItem) {
        return item.subItems !== undefined ? item.subItems.length > 0 : false;
    }

}
