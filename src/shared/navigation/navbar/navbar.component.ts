import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/core/services/auth.service';
import { LanguageService } from 'src/core/services/language.service';
import { NavigationService } from 'src/core/services/navigation.service';
import { ThemeService } from 'src/core/services/theme.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    constructor(
        public theme: ThemeService,
        public translate: TranslateService,
        public languageService: LanguageService,
        public navigationService: NavigationService,
        public authService: AuthService) { }

    ngOnInit(): void {
    }

}
