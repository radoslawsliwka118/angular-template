import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { NavbarComponent } from './navigation/navbar/navbar.component';
import { FooterComponent } from './navigation/footer/footer.component';
import { SidebarComponent } from './navigation/sidebar/sidebar.component';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { ToastrModule } from 'ngx-toastr';
import { HttpLoaderFactory } from 'src/app/app.module';
import { LanguageService } from 'src/core/services/language.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
    declarations: [        
        NavbarComponent,
        FooterComponent,
        SidebarComponent
    ],
    imports: [
        CommonModule,
        SharedRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    providers: [
        LanguageService
    ],
    exports: [
        CommonModule,
        SharedRoutingModule,
        NavbarComponent,
        FooterComponent,
        SidebarComponent,
        TranslateModule,
        ReactiveFormsModule,
        FormsModule,
    ]
})
export class SharedModule { }