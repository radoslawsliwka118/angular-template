import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { SharedModule } from 'src/shared/shared.module';


@NgModule({
    declarations: [],
    imports: [
        SharedModule,
        CommonModule,
        CoreRoutingModule
    ]
})
export class CoreModule { }
