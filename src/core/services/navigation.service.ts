import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class NavigationService {

    hasSidebar: boolean = false;
    collapseSidebar: boolean = false;
    mobileCollapseSidebar: boolean = false;
    onListHover: boolean = false;
    itemOnHover: number = -1;
    isMobileScreen: boolean;

    constructor() { }

    sidebarToggle(): void {    
        if(!this.isMobileScreen) {    
            this.collapseSidebar = !this.collapseSidebar;
        } else {
            this.mobileCollapseSidebar = !this.mobileCollapseSidebar;
        }
    }

    onHover(i:number): void{
        this.itemOnHover = i;
    }
}
