import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/auth.model';
import { exampleUser } from '../../auth/user-example';
import * as apiConfig from '../../config/apiConfig.model'
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    user: User;

    constructor(private http: HttpClient, private toastr: ToastrService, private translate: TranslateService, private router: Router) { 
        this.user = exampleUser;
    }

    // Returns the current user
    public getDetails(): User {
        this.http.get(apiConfig.authAPI.details).subscribe((res) => { 
            this.user = <any>res;
        })
        return this.user;
    }

    // Login
    public login(user) {
        this.user = exampleUser; //test
        
        this.http.post(apiConfig.authAPI.login, user).subscribe((res) => {
            this.user = this.getDetails();
            this.toastr.success(this.translate.instant('AUTH.TOAST.LOGIN'));

        }, (error) => {
            this.toastr.error(error.message);
        })
    }

    // Signup
    public signup(user) {
        this.http.post(apiConfig.authAPI.signup, user).subscribe((res) => {
            
            this.user = user;
            this.toastr.success(this.translate.instant('AUTH.TOAST.SIGNUP'));

        }, (error) => {
            this.toastr.error(error.message);
        })
    }

    // Recover
    public recover(user) {
        this.http.post(apiConfig.authAPI.recover, user).subscribe((res) => {
            
            this.toastr.success(this.translate.instant('AUTH.TOAST.RECOVER'));

        }, (error) => {
            this.toastr.error(error.message);
        })
    }

    // Logout
    public logout() {
        this.user = undefined; //test
        this.router.navigate(['/']) //test

        this.http.delete(apiConfig.authAPI.logout).subscribe((res) => {
            this.user = undefined;
            this.router.navigate(['/'])
            this.toastr.success(this.translate.instant('AUTH.TOAST.LOGOUT'));

        }, (error) => {
            this.toastr.error(error.message);
        })
    }

}
