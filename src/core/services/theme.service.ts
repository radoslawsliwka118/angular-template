import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ThemeService {

    constructor() { }

    changeTheme() {
        let theme = document.body.getAttribute('data-theme') === 'light' ? 'dark' : 'light';

        this.setThemeColor(theme);
    }
    setThemeColor(color){
        document.body.setAttribute('data-theme', color);
    }

}
