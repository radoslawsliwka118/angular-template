export class User {
    id: string;
    username: string;
    password: string;
    email: string;
    avatar: string; //base64 image
}
