import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/core/services/auth.service';
import { LanguageService } from 'src/core/services/language.service';
import Validation from 'src/core/services/validation';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    signUpForm: FormGroup;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        public languageService: LanguageService,
        public translate: TranslateService,
        private authService: AuthService) { }

    ngOnInit(): void {
        this.signUpForm = this.formBuilder.group(
            {
                username: [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(6),
                        Validators.maxLength(20)
                    ]
                ],
                email: ['', [Validators.required, Validators.email]],
                password: [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(6),
                        Validators.maxLength(20)
                    ]
                ],
                confirmPassword: ['', Validators.required],
                acceptTerms: [false, Validators.requiredTrue]
            },
            {
                validators: [Validation.match('password', 'confirmPassword')]
            }
        );
    }

    get f(): { [key: string]: AbstractControl } {
        return this.signUpForm.controls;
    }
    
    onSubmit(): void {
        this.submitted = true;
    
        if (this.signUpForm.invalid) {
            return;
        }
        
        this.authService.signup(JSON.stringify(this.signUpForm.value, null, 2));
    }

}
