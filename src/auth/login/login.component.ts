import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LanguageService } from 'src/core/services/language.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { ThemeService } from 'src/core/services/theme.service';
import { AuthService } from 'src/core/services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        public languageService: LanguageService, 
        public translate: TranslateService,
        private authService: AuthService, 
        public theme: ThemeService) { }

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group(
            {
                email: ['', [Validators.required, Validators.email]],
                password: [
                    '',
                    [
                        Validators.required,
                        Validators.minLength(6),
                        Validators.maxLength(20)
                    ]
                ]
            }
        );
    }

    get f(): { [key: string]: AbstractControl } {
        return this.loginForm.controls;
    }

    onSubmit(): void {
        this.submitted = true;
    
        if (this.loginForm.invalid) {
            return;
        }
        
        this.authService.login(JSON.stringify(this.loginForm.value, null, 2));
    }

}
