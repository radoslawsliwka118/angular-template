import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/core/services/auth.service';
import { LanguageService } from 'src/core/services/language.service';

@Component({
    selector: 'app-recover',
    templateUrl: './recover.component.html',
    styleUrls: ['./recover.component.scss']
})
export class RecoverComponent implements OnInit {
    recoverForm: FormGroup;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        public languageService: LanguageService,
        public translate: TranslateService,
        private authService: AuthService) { }

    ngOnInit(): void {
        this.recoverForm = this.formBuilder.group(
            {
                email: ['', [Validators.required, Validators.email]],
            }
        );
    }

    get f(): { [key: string]: AbstractControl } {
        return this.recoverForm.controls;
    }
    
    onSubmit(): void {
        this.submitted = true;
    
        if (this.recoverForm.invalid) {
            return;
        }
        
        this.authService.recover(JSON.stringify(this.recoverForm.value, null, 2))
    }

}
