import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from 'src/shared/shared.module';
import { SignupComponent } from './signup/signup.component';
import { RecoverComponent } from './recover/recover.component';
import { LoginComponent } from './login/login.component';


@NgModule({
    declarations: [
        LoginComponent,
        SignupComponent,
        RecoverComponent
    ],
    imports: [
        SharedModule,
        CommonModule,
        AuthRoutingModule
    ]
})
export class AuthModule { }
