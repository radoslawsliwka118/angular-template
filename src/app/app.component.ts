import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/core/services/navigation.service';
import { ThemeService } from 'src/core/services/theme.service';
import * as ThemeConfig from './../config/themeConfig.model'

@Component({
    selector: 'app-root',
    template: `<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit{
    title = 'angular-template';

    constructor( 
        private breakpointObserver: BreakpointObserver,
        private theme: ThemeService,
        private navigationService: NavigationService ) {}
    
    ngOnInit() {
        this.theme.setThemeColor(ThemeConfig.color);

        this.breakpointObserver
            .observe(['(min-width: 600px)'])
            .subscribe( (state: BreakpointState) => {
                if (state.matches) {
                    this.navigationService.isMobileScreen = false;
                    this.navigationService.mobileCollapseSidebar = false;
                }
                else {
                    this.navigationService.isMobileScreen = true;
                }
            });
    }
}
