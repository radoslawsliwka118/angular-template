import { Component, OnInit } from '@angular/core';
import { LanguageService } from 'src/core/services/language.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-notfound',
    templateUrl: './notfound.component.html',
    styleUrls: ['./notfound.component.scss']
})
export class NotfoundComponent implements OnInit {

    constructor(public languageService: LanguageService, public translate: TranslateService) { }

    ngOnInit(): void {
    }

}
