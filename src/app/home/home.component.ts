import { Component, OnInit } from '@angular/core';
import { ITEMS } from './sidebar-home-items'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  menuItems
  constructor() { }

  ngOnInit(): void {
      this.menuItems = ITEMS;
  }

}
