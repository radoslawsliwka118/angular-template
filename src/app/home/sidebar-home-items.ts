import { MenuItem } from '../../core/models/sidebar.model';

export const ITEMS: MenuItem[] = [
    {
        id: 1,
        label: 'HOME.SIDEBAR.MENU1',
        isTitle: true
    },
    {
        id: 2,
        label: 'HOME.SIDEBAR.ITEM1.TITLE',
        icon: 'fa fa-home',
        subItems: [
            {
                id: 3,
                label: 'HOME.SIDEBAR.ITEM1.SUBITEM1',
                link: 'page1',
                parentId: 2
            },
            {
                id: 4,
                label: 'HOME.SIDEBAR.ITEM1.SUBITEM2',
                link: 'page2',
                parentId: 2
            }
        ]
    },
];

