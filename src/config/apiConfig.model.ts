const authApiURL = '/auth'; //Auth server URL
const appApiURL = '/app'; //APP server URL

export const authAPI = {
    details: authApiURL + '/details',
    login: authApiURL + '/login',
    signup: authApiURL + '/signup',
    recover: authApiURL + '/recover',
    logout: authApiURL + '/logout'
};

export const appAPI = {};


